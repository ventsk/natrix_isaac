#include "natrix_driver.hpp"
#include "engine/gems/state/io.hpp"
#include "messages/state/differential_base.hpp"
#include "engine/gems/sight/sight.hpp"
#include "engine/core/logger.hpp"
#include <cstdio>

#include <iostream>
#include <unistd.h>
#include <fcntl.h>
//#include <sys/ioctl.h>
#include <termios.h> // Contains POSIX terminal control definitions
#include <errno.h>   // Error integer and strerror() function
#include <string.h>

namespace isaac
{
void NatrixDriver::start()
{
    init();
    tickBlocking();
    // tickPeriodically();
}

void NatrixDriver::tick()
{
#if 1
    if (rx_natrix_cmd().available())
    {
        // LOG_ERROR("Drive command received!");
        messages::DifferentialBaseControl command;
        ASSERT(FromProto(rx_natrix_cmd().getProto(), rx_natrix_cmd().buffers(),
                         command),
               "Failed to parse rx_segway_cmd");

        double safe_speed        = command.linear_speed();
        double safe_turning_rate = command.angular_speed();
#if 0
        if ((safe_speed > 0) || (safe_speed < 0))
        {
            std::printf("Foward: %.2f, turn: %.2f \n", safe_speed,
                        safe_turning_rate);
        }
#endif

        messages::DifferentialBaseDynamics natrix_state;
        natrix_state.linear_speed()         = safe_speed;
        natrix_state.angular_speed()        = safe_turning_rate;
        natrix_state.linear_acceleration()  = 0.f;
        natrix_state.angular_acceleration() = 0.0;
        ToProto(natrix_state, tx_natrix_state().initProto(),
                tx_natrix_state().buffers());
        tx_natrix_state().publish();

        safe_speed *= 1000;
        safe_turning_rate *= 1000;

        int16_t rspeed = 0;
        int16_t lspeed = 0;

        rspeed = safe_speed + safe_turning_rate;
        lspeed = safe_speed - safe_turning_rate;

        if (1000 < rspeed)
        {
            rspeed = 1000;
        }
        else if (-1000 > rspeed)
        {
            rspeed = -1000;
        }
        if (1000 < lspeed)
        {
            lspeed = 1000;
        }
        else if (-1000 > lspeed)
        {
            lspeed = -1000;
        }

        setSpeed(rspeed, lspeed);

#if 0
        if(0 < safe_speed)
        {
            setSpeed(500, 500);
        }
        else if(0 > safe_speed)
        {
            setSpeed(-500, -500);
        }
        else
        {
            setSpeed(0, 0);
        }
#endif
    }
#endif

#if 0
    // publish current state of natrix
    static float                       speed = 0.f;
    messages::DifferentialBaseDynamics natrix_state;
    natrix_state.linear_speed()         = speed;
    natrix_state.angular_speed()        = speed;
    natrix_state.linear_acceleration()  = 0.f;
    natrix_state.angular_acceleration() = 0.0;
    ToProto(natrix_state, tx_natrix_state().initProto(),
            tx_natrix_state().buffers());

    speed = speed + 1.f;
    if (10 <= speed)
    {
        speed = 0.f;
    }

    tx_natrix_state().publish();
#endif
    // LOG_ERROR("Odometry sent!");
    static char dbg_count = 0;
    show("plot_1", dbg_count++);
}

void NatrixDriver::stop()
{
    deinit();
}

int NatrixDriver::init()
{
    std::cout << "hello world" << std::endl;

    // Open the IoMCU port
    if ((serial_port_file = open("/dev/ttyACM0", O_RDWR)) < 0)
    {
        serial_port_file = 0;
        std::cout << "Failed to open the port" << std::endl;
        return -1;
    }
    std::cout << "IoMCU port is open" << std::endl;

    // Configure the COM port
    // Create new termios struc, we call it 'tty' for convention
    struct termios tty;
    memset(&tty, 0, sizeof tty);

    // Read in existing settings, and handle any error
    if (tcgetattr(serial_port_file, &tty) != 0)
    {
        printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));
        deinit();
        return -2;
    }
    tty.c_cflag &= ~PARENB; // Clear parity bit, disabling parity (most common)
    tty.c_cflag &= ~CSTOPB; // Clear stop field, only one stop bit used in
                            // communication (most common)
    tty.c_cflag |= CS8;     // 8 bits per byte (most common)
    tty.c_cflag &=
        ~CRTSCTS; // Disable RTS/CTS hardware flow control (most common)
    tty.c_cflag |=
        CREAD | CLOCAL;     // Turn on READ & ignore ctrl lines (CLOCAL = 1)
    tty.c_lflag &= ~ICANON; // Don't wait for new line char.
    tty.c_lflag &= ~ECHO;   // Disable echo
    tty.c_lflag &= ~ECHOE;  // Disable erasure
    tty.c_lflag &= ~ECHONL; // Disable new-line echo
    tty.c_lflag &= ~ISIG;   // Disable interpretation of INTR, QUIT and SUSP
    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR |
                     ICRNL); // Disable any special handling of received bytes
    tty.c_cc[VTIME] = 10;    // Wait for up to 1s (10 deciseconds), returning as
                             // soon as any data is received.
    tty.c_cc[VMIN] = 0;

    tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes
                           // (e.g. newline chars)
    tty.c_oflag &=
        ~ONLCR; // Prevent conversion of newline to carriage return/line feed

    cfsetispeed(&tty, B115200);
    cfsetospeed(&tty, B115200);

    // Save tty settings, also checking for error
    if (tcsetattr(serial_port_file, TCSANOW, &tty) != 0)
    {
        printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
        deinit();
        return -3;
    }

    std::cout << "COM port configured!" << std::endl;
    return 0;
}
void NatrixDriver::deinit()
{
    if (0 < serial_port_file)
    {
        close(serial_port_file);
        serial_port_file = 0;
    }
}
int NatrixDriver::setSpeed(int16_t rspeed, int16_t lspeed)
{
    if (0 == serial_port_file)
    {
        return -1;
    }

    char    serial_buffer[20];
    ssize_t tx_size          = 0;
    serial_buffer[tx_size++] = 's';
    sprintf(serial_buffer + tx_size, "%04X", lspeed + 1000);
    tx_size += 4;
    sprintf(serial_buffer + tx_size, "%04X", rspeed + 1000);
    tx_size += 4;
    serial_buffer[tx_size++] = '\n';

    //std::cout << lspeed + 1000 << " " << rspeed + 1000 << std::endl;

    if (tx_size == write(serial_port_file, serial_buffer, tx_size))
    {
        // Success
    }
    else
    {
        printf("Error %i from write: %s\n", errno, strerror(errno));
        deinit();
    }
    return 0;
}

} // namespace isaac