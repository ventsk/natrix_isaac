#pragma once

#include "engine/alice/alice_codelet.hpp"
#include "messages/state.capnp.h"

namespace isaac
{
class NatrixDriver : public isaac::alice::Codelet
{
public:
    /* Interface -------------------------------------------------------------*/
    void start() override;
    void tick() override;
    void stop() override;

    /* Incomming message channels --------------------------------------------*/
    // Linear and angular speed command for driving segway
    // (navigation::DifferentialBaseControl type)
    ISAAC_PROTO_RX(StateProto, natrix_cmd);

    /* Outgoing message channels ---------------------------------------------*/
    // State of the segway consisting of linear and angular speeds and
    // accelerations
    // (DifferentialBaseDynamics)
    ISAAC_PROTO_TX(StateProto, natrix_state);

private:
    int  serial_port_file = 0;
    int  init();
    void deinit();
    int  setSpeed(int16_t, int16_t);
};
} // namespace isaac

ISAAC_ALICE_REGISTER_CODELET(isaac::NatrixDriver);