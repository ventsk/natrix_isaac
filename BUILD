"""
Copyright (c) 2019, NVIDIA CORPORATION. All rights reserved.

NVIDIA CORPORATION and its licensors retain all intellectual property
and proprietary rights in and to this software, related documentation
and any modifications thereto. Any use, reproduction, disclosure or
distribution of this software and related documentation without an express
license agreement from NVIDIA CORPORATION is strictly prohibited.
"""

load("//engine/build:isaac.bzl", "isaac_app", "isaac_subgraph")

isaac_subgraph(
    name = "natrix_hardware_subgraph",
    modules = [
        "imu",
        "//apps/natrix/natrix_hardware:natrix_driver",
        "realsense",
        "viewers"
    ],
    subgraph = "natrix_hardware.subgraph.json",
    visibility = ["//visibility:public"],
)

isaac_subgraph(
    name = "2d_natrix_subgraph",
    data = [
        ":natrix_hardware_subgraph",
    ],
    modules = [
        "rgbd_processing",
    ],
    subgraph = "2d_natrix.subgraph.json",
    visibility = ["//visibility:public"],
)

isaac_app(
    name = "natrix",
    data = [
        ":2d_natrix_subgraph",
        "//apps/assets/maps",
        "//packages/navigation/apps:goal_generators_subgraph",
        "//packages/navigation/apps:differential_base_commander_subgraph",
        "//packages/navigation/apps:differential_base_navigation_subgraph",
    ],
    modules = [
        "map",
    ],
)